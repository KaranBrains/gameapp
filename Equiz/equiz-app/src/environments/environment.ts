export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyCxYRYfVBUpujtEs-yvSdoiZuA8qEo_C6o",
    authDomain: "pi-game-app.firebaseapp.com",
    databaseURL: "https://pi-game-app-default-rtdb.firebaseio.com/",
    projectId: "pi-game-app",
    storageBucket: "gs://pi-game-app.appspot.com",
    messagingSenderId: "267558723017",
    appId: "1:267558723017:android:d559243145de2e6fda9c18"
  },
  defaultAnswerCount: 4, //Answer input count,
  snackBarConfig: {
    duration: 2 * 1000
  },
  ONESIGNAL_APP_ID: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  ONESIGNAL_REST_API_KEY: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
  IS_TESTING: true,
  AD_UNIT_ID: "ca-app-pub-8520899743438874~1213176995"
};
